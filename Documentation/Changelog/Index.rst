
.. include:: /Includes.rst.txt

.. _changelog:

==========
Change log
==========

Version 11.1.4
--------------

- new version
- bugfix: Error 404 - Backend.js , see https://gitlab.com/bartista87/gridtocontainer/-/merge_requests/18, thanks to @schmidtwebmedia

Version 11.1.3
--------------

- new version
- small template fix, see https://gitlab.com/bartista87/gridtocontainer/-/merge_requests/16, thanks to @schmidtwebmedia

Version 11.1.2
--------------

- new version
- bugfix in composer.json

Version 11.1.1
--------------

- new version
- see issue: https://gitlab.com/bartista87/gridtocontainer/-/issues/11

Version 11.1.0
--------------

- new version
- change documentation

Version 11.0.6
--------------

- bugfix sameCid = 0
- new feature flexform support

Version 11.0.5
--------------

- small fix in composer.json for documantation, thanks to  @linawolf
- small fix in readme.rst for documantation, thanks to  @linawolf

Version 11.0.4
--------------

- adds correct documentation, thanks to  @linawolf
- fix: remove empty check on 'columnid' which results always in 0, thanks to @anjey.link


Version 11.0.3
--------------

- small fixes on ext_emconf.php
- small fixes on composer.json


Version 11.0.2
--------------

- migrate to typo3 v11 and refactor
- bugfix in migrategeneral-Action
- migrate backend templates and css to bootstrap 5
- include bugfix "Columns not migrate, when based on container with empty column."
- version to only typo3 v11

Thanks to the contributers: @stigfaerch and @florian.obwegs
